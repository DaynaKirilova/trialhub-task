import { useEffect, useState } from "react";
import "../App.css";

const QuoteOfTheDay = (props) => {
  const [quote, setQuote] = useState(null);
  useEffect(() => {
    fetch(`https://quotes.rest/qod?language=en`)
      .then((res) => res.json())
      .then(setQuote);
  }, []);

  return (
    <div className="quote-of-the-day">
      <div className="card">
        <h1 className="quote-heading">Quote of the day:</h1>
        <div className="close-btn" onClick={props.onClick}>
          x
        </div>
        <div className="quote-text">
          {quote && quote.contents.quotes[0].quote}
        </div>
      </div>
    </div>
  );
};
export default QuoteOfTheDay;
