import "../App.css";
import img from "../images/woman-modal-photo.jpg";
const HomePageModal = (props) => {
  return (
    <div className="modal-container">
      <div className="modal-container_body">
        <div className="modal-first-section">
          <div className="modal-first-section_heading">Hi there 👋 </div>
          <div
            className="modal-first-section_close_btn"
            onClick={props.onClick}
          >
            X
          </div>
          <div className="modal-first-section_text">
            {" "}
            Ask us anything or share your feedback.
          </div>
          <div className="modal-second-section">
            <b className="modal-second-section_heading">Start a conversation</b>
            <div className="modal-second-section_img_text">
              <img
                src={img}
                alt="modal image"
                className="modal-second-section_img"
              ></img>
              <div className="modal-second-section_text">
                We'll reply as soon as we can
              </div>
            </div>
            <div className="modal-second-section_btn">Send us a message</div>

            <div className="modal-second-section_see-all">
              See all your conversations
            </div>
          </div>
        </div>
        <div className="modal-third-section">
          <a
            href="https://www.intercom.com/intercom-link?user_id=60e44669f8fe2b4c148fd032&powered_by_app_id=i0u4wdsh&company=TrialHub&solution=live-chat&utm_content=4+home-screen+we-run-on-intercom&utm_source=desktop-web&utm_medium=messenger&utm_campaign=intercom-link&utm_referrer=nullblank"
            className="modal-third-section_text"
            target="_blank"
          >
            We run on Intercom
          </a>
        </div>
      </div>
    </div>
  );
};
export default HomePageModal;
