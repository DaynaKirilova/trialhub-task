import logo from "../../images/logo-white.png";
import banner from "../../images/banner.png";
import "../../App.css";
import HomePageModal from "../../components/HomePageModal";
import { useState } from "react";
import QuoteOfTheDay from "../../components/QuoteOfTheDay";

const HomePage = () => {
  const [contactModalIsOpen, setContactModalIsOpen] = useState(false);
  const [quoteModalIsOpen, setQuoteModalIsOpen] = useState(false);

  const showContactModal = () => {
    setContactModalIsOpen(true);
  };
  const showQuoteModal = () => {
    setQuoteModalIsOpen(true);
  };
  const closeContactModal = () => {
    setContactModalIsOpen(false);
  };
  const closeQuoteModal = () => {
    console.log("quote modal closing");
    setQuoteModalIsOpen(false);
  };

  return (
    <div className="page-container">
      <meta name="viewport" content="width=device-width, initial scale=1.0" />
      <header className="main-header">
        <div className="main-header_container">
          <div className="main-header_logo">
            <img
              src={logo}
              className="main-header_logo_img"
              alt="TrialHub Logo"
            />
            <a className="main-header_brand">TrialHub</a>
          </div>
          <nav className="main-nav">
            <ul className="main-nav-items">
              <li className="main-nav_item">
                <a className="main-nav_item_a" onClick={showContactModal}>
                  Contact Us
                </a>
                {contactModalIsOpen && (
                  <HomePageModal onClick={closeContactModal} />
                )}
              </li>
              <li className="main-nav_item">
                <a className="main-nav_item_a">Sign In</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
      <div>
        <div className="main-ribbon">
          <div className="container-ribbon_text">
            Find out how Covid-19 impacted the clinical research industry
            <a
              href="https://trialhub.findmecure.com/blog/whitepapers/recovering-from-covid-the-2021-clinical-research-market/"
              target="_blank"
            >
              <b>Get our 2021 Market Overview</b>
            </a>
          </div>
        </div>
      </div>
      <div className="main-page-above-the-separator">
        <div className="main-page_container">
          <div className="main-page_content">
            <div className="main-page_left">
              <div className="main-page_description">
                <h1 className="main-page_main_text">
                  Go Beyond Traditional Clinical Trial Feasibility
                </h1>
                <div className="main-page_subtext">
                  Why losing studies you can win with TrialHub
                </div>
              </div>
              <div className="main-page_email">
                <input
                  type="text"
                  className="main-page_email_field"
                  placeholder="Your work email"
                ></input>
                <button
                  type="button"
                  className="main-page_button_email"
                  onClick={showQuoteModal}
                >
                  See TrialHub in Action
                </button>
                {quoteModalIsOpen && (
                  <QuoteOfTheDay onClick={closeQuoteModal} />
                )}
              </div>
            </div>
            <div className="main-page_right">
              <div className="main-page_image">
                <img
                  className="main-page_image_banner"
                  src={banner}
                  alt="TrialHubBanner"
                  title="stop waisting your time on manual research"
                />
              </div>
            </div>
          </div>
          <div className="main-page_testimonial">
            <div className="main-page_testimonial_container">
              <b className="quote-mark">"</b>
              <div className="main-page_testimonial_text">
                Previously only the big CROs and pharma could afford such a
                sophisticated platform.
                <br />
                <b>TrialHub is a game changer!</b>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="main-page_separator"></div>
    </div>
  );
};
export default HomePage;
